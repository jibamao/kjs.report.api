﻿using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Kjs.Report.Admin.Handler
{
    /// <summary>
    /// swagger upload处理
    /// </summary>
    public class UploadFilter : IOperationFilter
    {
        /// <summary>
        /// swagger upload处理
        /// </summary>
        /// <param name="operation"></param>
        /// <param name="context"></param>
        public void Apply(Operation operation, OperationFilterContext context)
        {
            if (!string.IsNullOrWhiteSpace(operation.Summary) && operation.Summary.Contains("upload"))
            {
                operation.Consumes.Add("application/form-data");
                operation.Parameters.Add(new NonBodyParameter
                {
                    Name = "file",
                    In = "formData",
                    Required = false,
                    Type = "file",
                    Description = "file stream"
                });
            }
        }
    }
}
