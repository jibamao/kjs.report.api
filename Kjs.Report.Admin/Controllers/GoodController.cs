﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Kjs.Report.IService;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Authorization;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Kjs.Report.Admin.Controllers
{
    /// <summary>
    /// 商品
    /// </summary>
    [Produces("application/json")]
    [Authorize("Bearer")]
    [EnableCors("Any")]
    [Route("api/good")]
    public class GoodController : ApiControllerBase
    {
        private IGoodService _goodService;
        /// <summary>
        /// 构造方法
        /// </summary>
        /// <param name="goodService"></param>
        public GoodController(IGoodService goodService)
        {
            _goodService = goodService;
        }

        /// <summary>
        /// 按照商品分类统计在售商品数据明细
        /// </summary>
        /// <param name="orgIds">组织编号,多个用 , 隔开[英文]</param>
        /// <param name="pageSize">每页数量</param>
        /// <param name="pageIndex">当前页码</param>
        /// <param name="line_id">线路编号</param>
        /// <param name="point_id">点位编号</param>
        /// <returns></returns>
        [HttpGet("onsale/page/list")]
        public object GoodSaleGroupList(string orgIds = "", int pageSize = 20, int pageIndex = 1, int line_id = 0, int point_id = 0)
        {
            return _goodService.GoodSaleGroupList(pageSize, pageIndex, "", orgIds, line_id, point_id);
        }
    }
}
