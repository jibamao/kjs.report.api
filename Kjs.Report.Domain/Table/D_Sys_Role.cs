﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Kjs.Report.Domain.Table
{
    [Table("sys_role")]
    public class D_Sys_Role : BaseEntity<int>
    {
        public string role_name { get; set; }
        public string description { get; set; }
        public int? sort { get; set; }
        public int? add_user { get; set; }
        public DateTime? add_time { get; set; }
        public int? update_user { get; set; }
        public DateTime? update_time { get; set; }
        public string role_type { get; set; }
    }
}
