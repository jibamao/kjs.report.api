﻿using AutoMapper;
using Kjs.Report.IService;
using Kjs.Report.Model;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Kjs.Report.Business
{
    public class BLL_SysRole
    {
        private readonly ISysRoleService _roleService;
        private readonly ISysUserService _userService;
        private readonly IMapper _mapper;
        private readonly ClaimsPrincipal _admin;
        private readonly string _ip;

        public BLL_SysRole(ISysRoleService roleService, ISysUserService userService, IMapper mapper, ClaimsPrincipal admin, string ip)
        {
            _roleService = roleService;
            _userService = userService;
            _mapper = mapper;
            _admin = admin;
            _ip = ip;
        }

        #region 接口调用

        /// <summary>
        /// 获取单个角色
        /// </summary>
        /// <returns></returns>
        public async Task<M_Sys_Role> Get(int id)
        {
            var d_sys_role = await _roleService.Get(id);
            var m_sys_role = _mapper.Map<M_Sys_Role>(d_sys_role);
            return m_sys_role;
        }

        /// <summary>
        /// 获取角色列表
        /// </summary>
        /// <returns></returns>
        public async Task<Tuple<IList<M_Sys_Role>, int>> GetPageList(string keyWord, int pageIndex, int pageSize)
        {
            var tuple = await _roleService.GetPageList(keyWord, pageIndex, pageSize);
            var items = tuple.Item1;
            var role_list = _mapper.Map<List<M_Sys_Role>>(items);
            return new Tuple<IList<M_Sys_Role>, int>(role_list, tuple.Item2);
        }
             
        /// <summary>
        /// 获取所有角色
        /// </summary>
        /// <returns></returns>
        public async Task<List<M_Sys_Role>> GetList()
        {
            var d_sys_role_list = await _roleService.GetList();
            var list = _mapper.Map<List<M_Sys_Role>>(d_sys_role_list);
            return list;
        }

        #endregion
    }
}
