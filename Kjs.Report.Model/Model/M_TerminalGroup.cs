﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kjs.Report.Model
{
    /// <summary>
    /// 
    /// </summary>
    public class M_TerminalGroup
    {
        /// <summary>
        /// 设备序号
        /// </summary>
        public string terminal_no { get; set; }
        /// <summary>
        /// 本机编码
        /// </summary>
        public string name { get; set; }
        /// <summary>
        /// 总销量
        /// </summary>
        public int total_count { get; set; }
        /// <summary>
        /// 总销售额
        /// </summary>
        public decimal total_amount { get; set; }
        /// <summary>
        /// 地址
        /// </summary>
        public string address { get; set; }
    }
}
