﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kjs.Report.Model
{
    /// <summary>
    /// 
    /// </summary>
    public class M_CategoryGroup
    {
        /// <summary>
        /// 分类名称
        /// </summary>
        public string title { get; set; }
        /// <summary>
        /// 总销量
        /// </summary>
        public int total_count { get; set; }

        /// <summary>
        /// 总销售额
        /// </summary>
        public decimal total_amount { get; set; }
    }
}
