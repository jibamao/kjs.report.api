﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kjs.Report.Model
{
    /// <summary>
    /// 促销商品
    /// </summary>
    public class M_PromotionGoodTitleGroup
    {
        public string title { get; set; }
        public decimal sell_price { get; set; }
        public decimal promotion_price { get; set; }
        public int total_count { get; set; }
        public decimal total_amount { get; set; }
    }
}
