﻿using Kjs.Report.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Kjs.Report.Model
{
    /// <summary>
    /// 
    /// 订单状态 1：未付款  2：已付款 3：已完成 4：取消  5：已退款 6：退款中  7:异常订单 8：已过期
    /// </summary>
    public class M_TerminalStatusGroup
    {    /// <summary>
         /// 
         /// </summary>
        public string name { get; set; }
        public string terminal_no { get; set; }
        public string address { get; set; }
        public int status_1 { get; set; }
        public int status_2 { get; set; }
        public int status_3 { get; set; }
        public int status_4 { get; set; }
        public int status_5 { get; set; }
        public int status_6 { get; set; }
        public int status_7 { get; set; }
        public int status_8 { get; set; }
        public int count { get; set; }
        public string refundRate => Utils.GetPercent(status_5, count);

    }
}
