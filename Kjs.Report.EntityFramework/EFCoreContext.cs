﻿using Kjs.Report.Domain.Table;
using Microsoft.EntityFrameworkCore;

namespace Kjs.Report.EntityFramework
{
    public class EFCoreContext : DbContext
    {
        public EFCoreContext(DbContextOptions<EFCoreContext> options) : base(options)
        {
            //在此可对数据库连接字符串做加解密操作
        }

        public DbSet<D_Sys_User> SysUsers { get; set; }
        public DbSet<D_Sys_Role> SysRoles { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}
