﻿using Kjs.Report.Model.Enums;
using System;

namespace Kjs.Report.Infrastructure
{
    public class BusinessException: Exception
    {
        public BusinessException(string message, ExceptionCode code, Exception ex = null)
            : base(message, ex) => HResult = (int)code;
    }
}
