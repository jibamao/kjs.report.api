﻿using Kjs.Report.DB;
using Kjs.Report.IService;
using Kjs.Report.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Kjs.Report.Service
{
    public class TerminalService : ITerminalService
    {

        /// <summary>
        /// 按照时间分组统计终端明细
        /// </summary>
        /// <param name="pageSize">每页数量</param>
        /// <param name="pageIndex">当前页码</param>
        /// <param name="filedOrder">排序字段</param>
        /// <param name="orgIds">组织编号[多个用  , 隔开]</param>
        /// <param name="province_id">省编号</param>
        /// <param name="city_id">市编号</param>
        /// <param name="area_id">区编号</param>
        /// <param name="line_id">线路编号</param>
        /// <param name="point_id">点位编号</param>
        /// <param name="beginTime">开始时间</param>
        /// <param name="endTime">结束时间</param>
        /// <param name="IsSuper">是否为超级角色</param>
        /// <returns></returns>
        public PagedList<IList<M_TerminalTimeGroup>> TimeGroupList(int pageSize, int pageIndex, string filedOrder, int province_id, int city_id, int area_id, string orgIds,
            int line_id, int point_id, DateTime? beginTime, DateTime? endTime)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append(@"SELECT
	DATE_FORMAT(add_time, '%Y-%m-%d') time,
	COUNT(1) count
FROM
	terminals
WHERE
	is_delete = 0
");
            if (!string.IsNullOrWhiteSpace(orgIds))
                strSql.AppendFormat(" and org_id in ({0}) ", orgIds);


            if (province_id > 0)
                strSql.AppendFormat(" and province_id = {0} ", province_id);

            if (city_id > 0)
                strSql.AppendFormat(" and city_id = {0} ", city_id);

            if (area_id > 0)
                strSql.AppendFormat(" and area_id = {0} ", area_id);

            if (line_id > 0)
                strSql.AppendFormat(" and line_id = {0} ", line_id);

            if (point_id > 0)
                strSql.AppendFormat(" and point_id = {0} ", point_id);

            if (beginTime != null)
                strSql.AppendFormat(" and add_time >= '{0}' ", beginTime);

            if (endTime != null)
                strSql.AppendFormat(" and add_time < '{0}' ", endTime);

            strSql.Append(@" GROUP BY time ");

            if (string.IsNullOrWhiteSpace(filedOrder))
                filedOrder = " time DESC ";

            return PagedHelper.PagedList<M_TerminalTimeGroup>(strSql.ToString(), pageSize, pageIndex, filedOrder);

        }

    }
}
